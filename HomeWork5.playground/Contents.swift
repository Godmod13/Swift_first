import UIKit
//Задача 1
open class Shape {
    open func calculareArea() -> Double {
        fatalError("not implemented")
    }
    
    open func calculatePerimeter() -> Double {
       fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculareArea() -> Double {
        return height * width
    }
    override func calculatePerimeter() -> Double {
        return (height + width) * 2
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculareArea() -> Double {
        return .pi * radius * radius
    }
    override func calculatePerimeter() -> Double {
        return 2 * .pi * radius
    }
}

class Square: Shape {
    private let side: Double
    
    init(side:Double) {
        self.side = side
    }
    
    override func calculareArea() -> Double {
        return side * side
    }
    override func calculatePerimeter() -> Double {
        return side * 4
    }
}

var area: Double = 0
var perimeter: Double = 0
let shapes: [Shape] = [Rectangle(height: 3, width: 2), Circle(radius: 4), Square(side: 6.5)]

for shape in shapes {
    area += shape.calculareArea()
    perimeter += shape.calculatePerimeter()
}
print("Общая площадь всех фигур: \(area)")
print("Общий периметр всех фигур: \(perimeter)")


//Задача 2
func findIndexWithGenerics<T: Equatable>(valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let foundIndexString = findIndexWithGenerics (valueToFind: "лама", in: arrayOfString) {

    print("Индекс ламы: \(foundIndexString)")

}

let arrayOfDouble = [2.5, 4.5, 1.5, 7.4]

if let foundIndexDouble = findIndexWithGenerics (valueToFind: 7.4, in: arrayOfDouble) {

    print("Индекс числа 7.4: \(foundIndexDouble)")

}

let arrayOfInt = [2, 4, 1, 7]

if let foundIndexInt = findIndexWithGenerics (valueToFind: 4, in: arrayOfInt) {

    print("Индекс числа 4: \(foundIndexInt)")

}
