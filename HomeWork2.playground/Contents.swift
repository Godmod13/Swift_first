import UIKit

//Задача 1
var milkmanPhrase = "Молоко - это полезно"

print(milkmanPhrase)

//Задача 2
var milkPrice: Double = 3

//Задача 3
milkPrice = 4.20

//Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit =  milkPrice * Double(milkBottleCount!)
print(profit)

//Задача 5
var employeesList: Array = [String]()

employeesList = ["Иван", "Марфа", "Андрей", "Петр", "Геннадий"]

//Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours = 35

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)

//Дополнительное задание "под звездочкой"

var milkBottleCountExtra: Double? = 15
var profitExtra: Double = 0.0

if let foo = milkBottleCountExtra {
    profitExtra = milkPrice * foo
    print(profitExtra)
} else {
    print("Переменная milkBottleCount == nil ")
}
//Принудительное развертование тут не используем, т.к. если переменная milkBottleCountExtra будет nil, программа упадет на моменте вычисления profitExtra.
// Еще пример объявление переменной с помощью сложения двух других, одна из которых nil. Так же программа упадет. 
//var x: Int = 10
//var y: Int? = nil
//var z = x + y!

