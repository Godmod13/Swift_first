import UIKit


//Задача 1
extension Int {
    func digitSum() -> Int {
        guard self > 10 else {
            return self
        }
        let num = self % 10
        let next = self / 10
        return num + next.digitSum()
    }
}

print("\(731.digitSum())")


//Задача 2
extension String {
    var intValue: Int {
        return Int(self) ?? 0
    }
}

print("\("104".intValue)")
print("\("Text".intValue)")

//Задача 3
protocol ArithmeticOperation {
    func calculate() -> Double
}

class Multiply: ArithmeticOperation {
    let multiplicant: Double
    let multiplier: Double
    
    init(multiplicant: Double, multiplier: Double) {
        self.multiplicant = multiplicant
        self.multiplier = multiplier
    }
    
    func calculate() -> Double {
        return multiplicant * multiplier
    }
}

class Divide: ArithmeticOperation {
    let dividend: Double
    let divider: Double
    
    init(dividend: Double, divider: Double) {
        self.dividend = dividend
        self.divider = divider
    }
    
    func calculate() -> Double {
        return dividend / divider
    }
}

class Add: ArithmeticOperation {
    let x: Double
    let y: Double
    
    init(x: Double, y: Double) {
        self.x = x
        self.y = y
    }
    
    func calculate() -> Double {
        return x + y
    }
}

class Subtract: ArithmeticOperation {
    let x: Double
    let y: Double
    
    init(x: Double, y: Double) {
        self.x = x
        self.y = y
    }
    
    func calculate() -> Double {
        return x - y
    }
}

class Calculator {
    func perform(operation: ArithmeticOperation) {
        print("\(operation.calculate())")
    }
}

let calculator = Calculator()
let divide = Divide(dividend: 10, divider: 3)

calculator.perform(operation: divide)
