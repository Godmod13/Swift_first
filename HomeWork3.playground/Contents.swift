import UIKit

//Задание 1
func fibonacciNumber(count: Int) -> [Int] {
    var number1 = 1
    var number2 = 1
    var storyPointsNumbers = [number1,number2]
    
    guard count > 1 else { return [] }
    for _ in 0 ..< count - 2 {
        let number = number1 + number2
        number1 = number2
        number2 = number
        storyPointsNumbers.append(number)
    }
    
    return storyPointsNumbers
}

print(fibonacciNumber(count: 5))
print(fibonacciNumber(count: 7))

//Задание 2
let printArrayFor = { (array: [Int]) in
    for number in array {
        print(number)
    }
}

printArrayFor(fibonacciNumber(count: 5))
printArrayFor(fibonacciNumber(count: 7))
