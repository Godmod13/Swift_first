import UIKit

//Задача 1
var evenNumbers: [Int] = []

for num in stride(from: 0, through: 50, by: 2) {
    print("\(num)")
    evenNumbers.append(num)
}

var count: Int = 0
repeat {
    print("\(evenNumbers[count])")
    count += 1
} while count < 10

//Задача 2
let athlete1 = "Stalin"
let athlete2 = "Lenin"

enum Judge: String {
    case judge1 = "Ivanov"
    case judge2 = "Petrov"
    case judge3 = "Sidorov"
}

var results: [String: [Judge: Double]] = [athlete1: [.judge1: 9,
                                                     .judge2: 5,
                                                     .judge3: 10],
                                          athlete2: [.judge1: 6,
                                                     .judge2: 3,
                                                     .judge3: 9]]


for (athlete, judgeScores) in results {
    var totalScore: Double = 0

    for score in judgeScores.values {
        totalScore += score
    }
    
    let averageScore = totalScore / Double(judgeScores.count)
    print("\(athlete): \(averageScore)")
}
