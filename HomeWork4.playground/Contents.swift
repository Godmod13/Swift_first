import UIKit

//Задание 1
class TextField {
    let header: String
    let length: Int
    let placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
}

let nameField = TextField(header: "Name", length: 25, placeholder: "Type your name", code: 1, priority: "high")
let surnameField = TextField(header: "Surname", length: 25, placeholder: "Type your surname", code: 2, priority: "high")
let ageField = TextField(header: "Age", length: 3, placeholder: "Type your age", code: 3, priority: "middle")
let cityField = TextField(header: "City", length: 15, placeholder: "Type your city", code: 15, priority: "low")

//Задание 2
class TextField2 {
    let header: String
    let length: Int
    var placeholder: String
    let code: Int
    let priority: String
    
    init(header: String, length: Int, placeholder: String, code: Int, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
    
    func checkLength(with maxLength: Int) -> Bool {
        return length <= maxLength
    }
    
    func changePlaceholder(with placeholder: String) {
        self.placeholder = placeholder
    }
}

let maxLength: Int = 25

let goodTextField = TextField2(header: "City", length: 15, placeholder: "Type your city", code: 15, priority: "low")

let badTextField = TextField2(header: "Nationality", length: 30, placeholder: "Type your city", code: 15, priority: "low")

print("\(goodTextField.header) is valid \(goodTextField.checkLength(with: maxLength))")
print("\(badTextField.header) is valid \(badTextField.checkLength(with: maxLength))")

print("Initial placeholder - \(goodTextField.placeholder)")
goodTextField.changePlaceholder(with: "Type your best city")
print("New placeholder - \(goodTextField.placeholder)")

//Задача 3

class TextField3 {
    let header: String
    let length: Int
    var placeholder: String?
    var code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: String) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority
    }
    
    func checkLength(with maxLength: Int) -> Bool {
        return length <= maxLength
    }
    
    func changePlaceholder(with placeholder: String?) {
        self.placeholder = placeholder
    }
    
    func changeCode(with code: Int?) {
        self.code = code
    }
}

let ageTextField = TextField3(header: "Age", length: 3, priority: "middle")

//Задача 4
enum Priority: String {
    case high
    case middle
    case low 
}

class TextField4 {
    let header: String
    let length: Int
    var placeholder: String?
    var code: Int?
    let priority: String
    
    init(header: String, length: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.length = length
        self.placeholder = placeholder
        self.code = code
        self.priority = priority.rawValue
    }
    
    func checkLength(with maxLength: Int) -> Bool {
        return length <= maxLength
    }
    
    func changePlaceholder(with placeholder: String?) {
        self.placeholder = placeholder
    }
    
    func changeCode(with code: Int?) {
        self.code = code
    }
}

let nameField4 = TextField4(header: "Name", length: 25,  priority: .high)
let surnameField4 = TextField4(header: "Surname", length: 25, priority: .high)
let ageField4 = TextField4(header: "Age", length: 3, priority: .middle)
let cityField4 = TextField4(header: "City", length: 15, priority: .low)

//Задача 5
struct TextField5 {
    let header: String
    let length: Int
    var placeholder: String?
    var code: Int?
    
    func checkLength(with maxLength: Int) -> Bool {
        return length <= maxLength
    }
    
    mutating func changePlaceholder(with placeholder: String?) {
        self.placeholder = placeholder
    }
    
    mutating func changeCode(with code: Int?) {
        self.code = code
    }
}

let nameField5 = TextField5(header: "Name", length: 25)
let surnameField5 = TextField5(header: "Surname", length: 25, code: 2)
let ageField5 = TextField5(header: "Age", length: 3, code: 3)
let cityField5 = TextField5(header: "City", length: 15)
